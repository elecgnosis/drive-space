const { ipcRenderer, remote, shell } = require('electron');
const _ = require('lodash');
const Handlebars = require('handlebars');
const Volume = require('../volume');

const selfWin = remote.getCurrentWindow();

const volumeCache = [];

const totalize = function totalizeImpl(volumes) {
  const WMIVolumeTotal = {
    DeviceID: 'total',
    Capacity: 0,
    FreeSpace: 0,
  };

  volumes.forEach((volume) => {
    WMIVolumeTotal.Capacity += volume.sizeRaw;
    WMIVolumeTotal.FreeSpace += volume.freeRaw;
  });
  return new Volume(WMIVolumeTotal);
};

const scrubDriveData = function scrubDriveDataImpl(volumes) {
  console.log('scrubDriveData()');
  const cardHeight = 59;
  const menuHeight = 26;
  const cardRowCount = volumes.length % 2 ? (volumes.length / 2) + 0.5 : volumes.length / 2;
  const maxHeight = menuHeight + (cardRowCount * cardHeight);
  // if new maxHeight is greater than minimumsize(height), set new maxHeight.
  // otherwise, set maximumsize(height) to minimumsize(height).
  // only change current height if higher than max.

  if (maxHeight > selfWin.getMinimumSize()[1]) {
    console.log('maxHeight > getMaximumSize');
    selfWin.setMaximumSize(selfWin.getMaximumSize()[0], maxHeight);
  } else {
    console.log('maxHeight < getMaximumSize');
    selfWin.setMaximumSize(selfWin.getMaximumSize()[0], selfWin.getMinimumSize()[1]);
  }
  if (selfWin.getSize()[1] > selfWin.getMaximumSize()[1]) {
    console.log('size > getMaximumSize');
    selfWin.setSize(selfWin.getMaximumSize()[0], selfWin.getMaximumSize()[1]);
  }
  return volumes
    // System Volumes can not be used, so filter them out.
    // Drives without a driveLetter or mount point are not strictly available, so filter them out.
    // TODO: Maybe change that with a setting.
    .filter(volume => !volume.SystemVolume && volume.Caption !== volume.DeviceID)
    .map(volume => new Volume(volume))
    .sort((a, b) => {
      // TODO: Is there any way to optimize this sorting algorithm?
      // a driveLetter is not empty, b driveLetter is not empty: compare driveLetters
      if (!_.isEmpty(a.driveLetter) && !_.isEmpty(b.driveLetter)) {
        return a.driveLetter.localeCompare(b.driveLetter, 'en-US');
      }
      // a driveLetter is not empty, b driveLetter is empty: a wins
      if (!_.isEmpty(a.driveLetter) && _.isEmpty(b.driveLetter)) {
        return -1;
      }
      // a driveLetter is empty, b is not empty: b wins
      if (_.isEmpty(a.driveLetter) && !_.isEmpty(b.driveLetter)) {
        return 1;
      }
      // a driveLetter is empty, b driveLetter is empty:
      //   a label is not empty, b label is not empty: compare labels
      if (!_.isEmpty(a.label) && !_.isEmpty(b.label)) {
        return a.label.localeCompare(b.label, 'en-US');
      }
      //   a label is empty, b label is not empty: b wins
      if (!_.isEmpty(a.label) && _.isEmpty(b.label)) {
        return -1;
      }
      //   a label is not empty, b label is empty: a wins
      if (_.isEmpty(a.label) && !_.isEmpty(b.label)) {
        return 1;
      }
      //   a label is empty, b label is empty: a wins by default
      return -1;
    });
};

const handleRefreshVolumesAsyncReply = function handleRefreshVolumesAsyncReplyImpl(event, reply) {
  // TODO: handle error replies from ipc reply
  const eleVolumeGrid = document.querySelector('.volume-grid');
  const volumeCardCompiled = Handlebars.templates['volumecard.html'];

  // TODO: Compare results on hand to incoming results, only change those that have changed,
  // and perform some kind of animated highlight effect to show those that have changed
  _.remove(volumeCache, () => true);
  scrubDriveData(reply).forEach(volume => volumeCache.push(volume));

  const volumeCardsHTML = volumeCache.map(card => volumeCardCompiled(card)).join('');
  eleVolumeGrid.innerHTML = volumeCardsHTML;
  eleVolumeGrid.querySelectorAll('.volume-designation').forEach((element) => {
    element.addEventListener('click', (eve) => {
      shell.openItem(eve.currentTarget.getAttribute('id'));
    });
  });
};

const togglePinToTopIcon = function togglePinToTopIconImpl(set = null) {
  const elePin = document.querySelector('.pin-to-top');
  const elePinIcon = elePin.querySelector('.mdi');
  const title = 'title';
  const unpinfromtop = 'Unpin from Top';
  const pintotop = 'Pin to Top';
  const mdipin = 'mdi-pin';
  const mdipinoff = 'mdi-pin-off';
  // TODO: There must be a better way to implement this logic chain.
  if (set === null) {
    // if no setting is provided, algorithm assumes that you want to set pin appearance to show
    // deactivation, including the tooltip explaining what the next click on it will do.
    if (selfWin.isAlwaysOnTop()) {
      elePin.setAttribute(title, unpinfromtop);
      elePinIcon.classList.remove(mdipin);
      elePinIcon.classList.add(mdipinoff);
    } else {
      elePin.setAttribute(title, pintotop);
      elePinIcon.classList.remove(mdipinoff);
      elePinIcon.classList.add(mdipin);
    }
  } else if (set) {
    elePin.setAttribute(title, unpinfromtop);
    elePinIcon.classList.remove(mdipin);
    elePinIcon.classList.add(mdipinoff);
  } else {
    elePin.setAttribute(title, pintotop);
    elePinIcon.classList.remove(mdipinoff);
    elePinIcon.classList.add(mdipin);
  }
};

const handlePinToTopClick = function handlePinToTopClickImpl() {
  console.log('handlePinToTopClick');
  if (selfWin.isAlwaysOnTop()) {
    selfWin.setAlwaysOnTop(false);
    togglePinToTopIcon();
  } else {
    selfWin.setAlwaysOnTop(true);
    togglePinToTopIcon();
  }
  ipcRenderer.send('saveAlwaysOnTop', selfWin.isAlwaysOnTop());
};

// run the app when the window is ready.
document.addEventListener('DOMContentLoaded', () => {
  togglePinToTopIcon(selfWin.isAlwaysOnTop());
  selfWin.on('resize', () => { ipcRenderer.send('window-resized', selfWin.getSize()); });
  selfWin.on('move', () => { ipcRenderer.send('window-moved', selfWin.getPosition()); });

  ipcRenderer.on('refreshVolumesAsyncReply', handleRefreshVolumesAsyncReply);

  const eleRefresh = document.querySelector('.refresh');
  eleRefresh.addEventListener('click', () => ipcRenderer.send('refreshVolumesAsync'));
  const eleMinimize = document.querySelector('.minimize');
  eleMinimize.addEventListener('click', () => selfWin.minimize());
  const eleClose = document.querySelector('.close');
  eleClose.addEventListener('click', () => selfWin.close());
  const elePin = document.querySelector('.pin-to-top');
  elePin.addEventListener('click', handlePinToTopClick);

  ipcRenderer.send('refreshVolumesAsync');
  ipcRenderer.on('setAlwaysOnTop', (event, arg) => {
    togglePinToTopIcon(arg);
  });
});
