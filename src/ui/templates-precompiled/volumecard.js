(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['volumecard.html'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"volume-card\">\r\n  <div class=\"left-box\" title=\"Free\">\r\n    <div class=\"big-number\">\r\n      "
    + alias4(((helper = (helper = helpers.freePretty || (depth0 != null ? depth0.freePretty : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"freePretty","hash":{},"data":data}) : helper)))
    + "\r\n    </div>\r\n    <div class=\"big-number-units\">\r\n      "
    + alias4(((helper = (helper = helpers.freeUnits || (depth0 != null ? depth0.freeUnits : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"freeUnits","hash":{},"data":data}) : helper)))
    + "\r\n    </div>\r\n    <div class=\"context-button\">\r\n      &#8943\r\n    </div>\r\n  </div>\r\n  <div class=\"right-box\">\r\n    <div class=\"volume-designation\" id=\""
    + alias4(((helper = (helper = helpers.caption || (depth0 != null ? depth0.caption : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"caption","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\">\r\n      <span class=\"volume-mount\">\r\n        ("
    + alias4(((helper = (helper = helpers.driveLetter || (depth0 != null ? depth0.driveLetter : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"driveLetter","hash":{},"data":data}) : helper)))
    + ")\r\n      </span>\r\n      <span class=\"volume-label\">\r\n        "
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "\r\n      </span>\r\n    </div>\r\n    <div class=\"meter\">\r\n      <meter\r\n        min=\"0\"\r\n        max=\""
    + alias4(((helper = (helper = helpers.sizeRaw || (depth0 != null ? depth0.sizeRaw : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sizeRaw","hash":{},"data":data}) : helper)))
    + "\"\r\n        high=\""
    + alias4(((helper = (helper = helpers.sizeThreeQuarters || (depth0 != null ? depth0.sizeThreeQuarters : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sizeThreeQuarters","hash":{},"data":data}) : helper)))
    + "\"\r\n        value=\""
    + alias4(((helper = (helper = helpers.usedRaw || (depth0 != null ? depth0.usedRaw : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"usedRaw","hash":{},"data":data}) : helper)))
    + "\"\r\n        ></meter>\r\n    </div>\r\n    <div class=\"small-numbers\">\r\n      <span class=\"small-number-left\" title=\"Used\">\r\n        "
    + alias4(((helper = (helper = helpers.usedPretty || (depth0 != null ? depth0.usedPretty : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"usedPretty","hash":{},"data":data}) : helper)))
    + alias4(((helper = (helper = helpers.usedUnits || (depth0 != null ? depth0.usedUnits : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"usedUnits","hash":{},"data":data}) : helper)))
    + "\r\n      </span>/\r\n      <span class=\"small-number-right\" title=\"Capacity\">\r\n        "
    + alias4(((helper = (helper = helpers.sizePretty || (depth0 != null ? depth0.sizePretty : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sizePretty","hash":{},"data":data}) : helper)))
    + alias4(((helper = (helper = helpers.sizeUnits || (depth0 != null ? depth0.sizeUnits : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sizeUnits","hash":{},"data":data}) : helper)))
    + "\r\n      </span>\r\n    </div>\r\n  </div>\r\n</div>\r\n";
},"useData":true});
})();