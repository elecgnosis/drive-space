const byter = require('byte-converter').converterBase2;
const findLowestUnit = require('./utilities/unit-calculator');

module.exports = class Volume {
  /**
   * Returns an object to send volume information to client. Prettied up and stripped down to just
   * the relevant fields.
   * @param  {Object} wmiVolume WMI Volume object.
   * @return {Object}           Volume object with just the fields needed.
   */
  constructor(wmiVolume) {
    if (!Object.hasOwnProperty.call(wmiVolume, 'DeviceID')) {
      throw new TypeError('volume data does not have a property named DeviceID.');
    }
    if (wmiVolume.DeviceID.trim() === '') {
      throw new TypeError('volume.DeviceID is blank.');
    }
    this.driveLetter = wmiVolume.DriveLetter || '';
    this.label = wmiVolume.Label || '';
    this.deviceId = wmiVolume.DeviceID || null;
    this.bootDrive = wmiVolume.BootVolume || false;
    this.caption = wmiVolume.Caption || '';
    this.sizeRaw = wmiVolume.Capacity || 0;
    this.sizeUnits = findLowestUnit(this.sizeRaw);
    this.sizePretty = byter(this.sizeRaw, 'B', this.sizeUnits).toFixed(2);
    this.sizeThreeQuarters = (this.sizeRaw * 0.92).toFixed();
    this.freeRaw = wmiVolume.FreeSpace || 0;
    this.freeUnits = findLowestUnit(this.freeRaw);
    this.freePretty = byter(this.freeRaw, 'B', this.freeUnits).toFixed(2);
    this.freePercent = ((this.freeRaw / this.sizeRaw) * 100).toFixed(2);
    this.isDataPresent = this.freePercent < 100;
    this.usedRaw = this.isDataPresent ? this.sizeRaw - this.freeRaw : 0;
    this.usedUnits = this.isDataPresent ? findLowestUnit(this.usedRaw) : '';
    this.usedPretty = this.isDataPresent ? byter(this.usedRaw, 'B', this.usedUnits).toFixed(2) : 0;
    this.usedPercent = this.isDataPresent ? (100 - this.freePercent).toFixed(2) : 0;
  }
};
