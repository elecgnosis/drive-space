const path = require('path');
const fs = require('fs');
const defaultWindowOptions = require('../window-options');

module.exports = class SettingsSerivce {
  /**
   * [constructor description]
   * @param  {[type]} userDataPath [description]
   * @return {[type]}              [description]
   */
  constructor(userDataPath) {
    console.log('SettingsService');
    this.userDataPath = userDataPath;
    this.drivespaceinifilename = path.resolve(this.userDataPath, 'drivespace.ini');
    this.settings = { windowSettings: defaultWindowOptions };
    try {
      const userSettings = JSON.parse(fs.readFileSync(this.drivespaceinifilename));
      this.settings = Object.assign(this.settings, userSettings);
    } catch (e) {
      console.log('Unable to load drivespace.ini, using default options', e.message);
    }
  }

  /**
   * Simple getter for settings object.
   * @return {Object} All user settings.
   */
  getSettings() {
    console.log('getSettings');
    return this.settings;
  }

  /**
  * [getWindowSettings description]
  * @return {[type]} [description]
  */
  getWindowSettings() {
    console.log('getWindowSettings');
    return this.settings.windowSettings;
  }

  /**
   * [saveUserSettings description]
   * @return {[type]} [description]
   */
  saveUserSettings() {
    console.log('saveUserSettings');
    try {
      fs.writeFileSync(this.drivespaceinifilename, JSON.stringify(this.settings));
      console.log('user settings saved:', this.settings);
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * [updateWindowSize description]
   * @param  {[type]} dimensions [description]
   * @return {[type]}            [description]
   */
  setWindowSize(dimensions) {
    console.log('updateWindowSize', dimensions);
    this.settings.windowSettings.width = dimensions[0];
    this.settings.windowSettings.height = dimensions[1];
  }

  /**
   * [updateWindowPosition description]
   * @param  {[type]} position [description]
   * @return {[type]}          [description]
   */
  setWindowPosition(position) {
    console.log('updateWindowPosition', position);
    this.settings.windowSettings.x = position[0];
    this.settings.windowSettings.y = position[1];
  }

  /**
   * [updateAlwaysOnTop description]
   * @param  {[type]} alwaysOnTop [description]
   * @return {[type]}             [description]
   */
  setAlwaysOnTop(alwaysOnTop) {
    console.log('updateAlwaysOnTopImpl', alwaysOnTop);
    this.settings.windowSettings.alwaysOnTop = alwaysOnTop;
  }
};
