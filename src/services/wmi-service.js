const nwmi = require('node-wmi');

// TODO: Services should perhaps be singletons that do not even need to be instantiated
module.exports = class WMIService {
  /**
   * [constructor description]
   * @return {[type]} [description]
   */
  constructor() {
    console.log('WMIService');
    this.wmiQuery = { class: 'win32_volume' };
  }

  /**
   * [getWMIVolumes description]
   * @return {Promise} [description]
   */
  getWMIVolumes() {
    return new Promise((resolve, reject) => {
      nwmi.Query(this.wmiQuery, (err, volumes) => {
        if (err) {
          // TODO: Collect system logs on disk
          console.error(err.message);
          reject(err);
        }
        resolve(volumes);
      });
    });
  }

  /**
   * [refreshVolumes description]
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  refreshVolumes(event) {
    console.log('refreshVolumes');
    // TODO: make the value returned to the requestor uniform?
    this.getWMIVolumes()
      .then((volumes) => {
        event.returnValue = volumes;
        console.log(volumes);
      })
      .catch((err) => { event.returnValue = err; });
  }

  /**
   * [refreshVolumesAsync description]
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  refreshVolumesAsync(event) {
    console.log('refreshVolumesAsync');
    // TODO: add command queue to prevent hammering
    this.getWMIVolumes()
      .then(volumes => event.sender.send('refreshVolumesAsyncReply', volumes))
      .catch(err => event.sender.send('refreshVolumesAsyncReply', err));
  }
};
