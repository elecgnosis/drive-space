const { app, BrowserWindow, Tray, Menu, MenuItem, ipcMain } = require('electron');
const path = require('path');
const url = require('url');
const SettingsService = require('./services/settings-service');
const WMIService = require('./services/wmi-service');

const settingsService = new SettingsService(app.getPath('userData'));
const wmiService = new WMIService();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let windowProcess;
let tray;
const alwaysOnTopMenuItem = new MenuItem({
  label: 'Pinned to Top',
  type: 'checkbox',
  click: () => {
    const isAlwaysOnTopToSet = !windowProcess.isAlwaysOnTop();
    settingsService.setAlwaysOnTop(isAlwaysOnTopToSet);
    windowProcess.setAlwaysOnTop(isAlwaysOnTopToSet);
    windowProcess.webContents.send('setAlwaysOnTop', isAlwaysOnTopToSet);
  },
});

const exitMenuItem = new MenuItem({
  label: 'Exit',
  type: 'normal',
  role: 'quit',
});

const trayTitleMenuItem = new MenuItem({
  label: 'Drive Space',
  type: 'normal',
});

const setupTray = function setupTrayImpl() {
  tray = new Tray('src/ico/hard-drive-dark.ico');
  tray.on('click', () => {
    windowProcess.focus();
  });
  tray.setToolTip('Drive Space');
  const trayMenu = new Menu();

  trayMenu.append(trayTitleMenuItem);
  trayMenu.append(alwaysOnTopMenuItem);
  trayMenu.append(exitMenuItem);
  tray.setContextMenu(trayMenu);
};

const createWindow = function createWindowImpl() {
  console.log('createWindow');
  const windowSettings = settingsService.getWindowSettings();
  alwaysOnTopMenuItem.checked = windowSettings.alwaysOnTop;
  // Create the browser window. use saved window options if available.
  windowProcess = new BrowserWindow(windowSettings);

  // load the index.html of the app.
  windowProcess.loadURL(url.format({
    pathname: path.join(__dirname, './ui/ui.html'),
    protocol: 'file:',
    slashes: true,
  }));

  // Open the DevTools.
  windowProcess.webContents.openDevTools();

  // Emitted when the window is closed.
  windowProcess.on('closed', () => {
    console.log('windowProcess.onclosed event');
    settingsService.saveUserSettings();
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    windowProcess = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  console.log('app.onready event');
  setupTray();
  createWindow();
});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  console.log('app.onwindow-all-closed event');
  app.quit();
});

app.on('activate', () => {
  console.log('app.onactivate event');
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (windowProcess === null) {
    createWindow();
  }
});

// still don't know if this is where ipcMain calls should go.
ipcMain.on('window-resized', (event, size) => {
  settingsService.setWindowSize(size);
});
ipcMain.on('window-moved', (event, position) => {
  settingsService.setWindowPosition(position);
});
ipcMain.on('saveAlwaysOnTop', (event, alwaysOnTop) => {
  // can't hit both of these things without havin access to both of their targets
  alwaysOnTopMenuItem.checked = alwaysOnTop;
  settingsService.setAlwaysOnTop(alwaysOnTop);
});
ipcMain.on('refreshVolumes', (event) => {
  wmiService.refreshVolumes(event);
});
ipcMain.on('refreshVolumesAsync', (event) => {
  wmiService.refreshVolumesAsync(event);
});
