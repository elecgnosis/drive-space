module.exports = function findLowestUnit(bytes) {
  let modBytes = bytes;
  let units = 'B';
  const unitNames = ['KB', 'MB', 'GB', 'TB'];

  for (let iter = 0; modBytes / 1024 > 0.90; iter += 1) {
    modBytes /= 1024;
    units = unitNames[iter];
  }
  return units;
};
